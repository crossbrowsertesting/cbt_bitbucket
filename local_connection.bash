#Used to start a local connection to CBT

#!/bin/bash -e

wget https://github.com/crossbrowsertesting/cbt-tunnel-nodejs/releases/download/v0.9.9/cbt_tunnels-linux-x64.zip
unzip cbt_tunnels-linux-x64.zip
./
sleep 10
