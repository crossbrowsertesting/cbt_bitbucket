from flask import Flask

#Create app server
app = Flask(__name__)

#Route to index page
@app.route('/')
def hello_world():
    return 'Hello, World!'

#Run the server
app.run()
