# CBT - Bitbucket Pipelines
This is an example of how you can use CBT [Automated Testing](https://crossbrowsertesting.com/automated-testing) with bitbucket pipelines to test a local development server. Python and Flask are used in this example but this can be done in a variety of languages and frameworks.

Bitbucket pipelines works by creating a Docker containers. Inside these containers you can run commands (like you might on a local machine) but with all the advantages of a fresh system, custom configured for your needs. To learn more about Docker containers you can go here: [More about Docker Containers](https://www.docker.com/resources/what-container)

### Requirements:
1. Bitbucket account
2. This repo
3. CBT username and authkey which can be found here: [CBT Credentials](https://app.crossbrowsertesting.com/selenium/run)

### Setup:
Setting up CBT with Bitbucket pipelines is quite simple, we'll be up and running in a few quick steps!

1. Clone this repo
     - `git clone https://crossbrowsertesting@bitbucket.org/crossbrowsertesting/cbt_bitbucket.git`
2. Enable Bitbucket pipelines in your newly cloned repository.
     - Go to: `Settings --> Pipelines Settings --> Enable Pipelines`
3. Add repository variables to your repo using your CBT Credentials.
     - Go to: `Settings --> Pipelines Repository variables`
     - Add CBT username: `CBT_USERNAME` | `add_username_here` (Replace @ with %40)
     - Add CBT authkey: `CBT_AUTHKEY` | `add_authkey_here`

Now every time you you push a commit the test will automatically start under the *Pipelines* tab
### Build configuration:
```
image: python:3.7.3

pipelines:
  default:
    - step:
        script:
          - apt-get install unzip
          - pip install flask
          - pip install selenium
          - pip install requests
          - python app.py /dev/null &
          - bash ./local_connection.bash
          - ./cbt_tunnels-linux-x64 --username $CBT_USERNAME --authkey $CBT_AUTHKEY --kill flag.txt /dev/null &
          - python CBT_test.py
          - bash ./kill_local_connection.bash
```

#### Lets see what this is doing:
- We are creating a container with Python 3.7.3 installed
- Next we get the required packages that we need:
  - `Unzip` (Used to unzip local connection package from CBT)
  - `Flask` (Simple webserver)
  - `Selenium` (Create webdriver for automation testing)
  - `Requests` (Send HTTP requests to CBT)
- We spin up a Flask server on port 5000
- We get the local connection package from CBT and run it using our credentials. [Local Connection Service](https://crossbrowsertesting.com/live-testing/local-testing)
- We run a simple test to connect to the website and check the page title.
- Finally we create the `flag.txt` file to then kill the local connection.
