from selenium import webdriver
from requests import get
import time
import os

class Automated_test:

    #Set up test
    def __init__(self, username, authkey):

        #Account credentials
        self.username = username
        self.authkey = authkey

        #Wait to allow tunnel and server fully setup
        print ("Waiting for local tunnel to start...")
        time.sleep(10)

    #Run simple test
    def run_test(self, test_url):

        #Capabilities
        caps = {}

        caps['name'] = 'Basic Test Example'
        caps['build'] = '1.0'
        caps['browserName'] = 'Chrome'
        caps['version'] = '74x64'
        caps['platform'] = 'WIN10'
        caps['screenResolution'] = '1366x768',
        caps['record_video'] = True


        #Create remote driver to CBT
        print ("Connecting to CBT...")
        self.driver = webdriver.Remote(
            desired_capabilities = caps,
            command_executor = "http://%s:%s@hub.crossbrowsertesting.com:80/wd/hub"%(self.username, self.authkey),
        )

        #Go development server
        print("Loading webpage...")
        self.driver.get(test_url)

        #Check if title is correct
        print("Checking url...")

        if self.driver.current_url == "http://127.0.0.1:5000/":
            self.test_result = "passed"
        else:
            self.test_result = "failed"

        time.sleep(2)

        #Kill the test
        print ("Closing session")
        self.driver.quit()

        #Print results
        print ("Your test has "+self.test_result)

    #Run simple test
    def kill_local_tunnel():
        #Create kill file
        kill_flag = open("./flag.txt","w+")
        kill_flag.close()

#Run the test
Automated_test(os.environ['CBT_USERNAME'], os.environ['CBT_AUTHKEY']).run_test('http://127.0.0.1:5000/')
Automated_test.kill_local_tunnel()
